// 『はじめてのJavascript』第三版の第四章で例示されるゲーム
let funds = 50;  // 所持金

// mからnの数字のランダムの整数を返す
const rand = (m, n) => {
    return m + Math.floor((n - m + 1) * Math.random());
}

// "crown", "anchor", "heart", "spade", "club", "diamond"いすれかを返す
const randFace = () => {
    return ["crown", "anchor", "heart", "spade", "club", "diamond"][rand(0, 5)];
}

// サイコロを三回振って、"crown", "anchor", "heart", "spade", "club", "diamond"
// の中から３つ（重複あり）handリストに格納し、返す
const roll = () => {
    const hand = [];

    for(let i = 0;i < 3;i++){
        hand.push(randFace());
    }
    console.log(hand);
    return hand;
}

// 賭け金決定
const decide_bets = () => {
    const bets = {"crown": 0, "anchor": 0, "heart": 0, "spade": 0, "club": 0, "diamond": 0};
    let totalBet = rand(1, funds);

    if(totalBet === 7){
        totalBet = funds;
        bets["heart"] = totalBet;

    }else{
        // 賭け金をランダムに振り分ける
        let remaining = totalBet;

        do{
            let bet = rand(1, remaining);
            let face = randFace();
            bets[face] = bets[face] + bet;
            remaining = remaining - bet;
        }while(remaining > 0);
    }
    // グローバル変数だよ。スコープが広くてキモいね
    funds = funds - totalBet;
    console.log(bets);
    return bets;
}

// 払戻金の計算
const calc_winning = (hands, bets) => {
    let return_coin = 0;
    for(let die = 0;die < hands.length; die++){
        let face = hands[die];
        if(bets[face] > 0){return_coin += bets[face]};
    }
    return return_coin;
}

const play = () => {
    // 所持金額が0より多い、もしくは100未満でゲーム続行。
    while(funds > 0 && funds < 100){
        let ret = calc_winning(roll(), decide_bets());
        funds = funds + ret;
        console.log(`現在の所持金は${funds}です。`);
    }
    if(funds == 0){console.log("破産しました");}
    if(funds >= 1000){console.log("所持金は100以上に増えました！");}
    return funds;
}

let count = 0;
let result = 0;

while(result == 0){
    console.log("-------------------------------------------------------------------------");
    count += 1;
    result = play();
    funds = 50;
}

console.log("勝った！");
console.log("破産回数" + (count - 1) + "回");
